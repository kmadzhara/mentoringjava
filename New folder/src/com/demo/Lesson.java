package com.demo;

import java.util.ArrayList;
import java.util.Date;

public class Lesson {
	private String lesson;
    private Date date;
    private Date duration;
    private Tutor tutor;
    private ArrayList<Student> students = new ArrayList<Student>();
    private Coordinator coordinator;
    private double price = 0.0;

    public Lesson(String lesson, Date date, Date duration, Tutor tutor) {
    	this.lesson = lesson;
        this.date = date;
        this.duration = duration;
        this.tutor = tutor;
    }
    
	public String getLesson() {
		return lesson;
	}

	public void setLesson(String lesson) {
		this.lesson = lesson;
	}

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDuration() {
        return duration;
    }

    public void setDuration(Date duration) {
        this.duration = duration;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setDuration(Tutor tutor) {
        this.tutor = tutor;
    }

    public Coordinator getCoordinator() {
        return coordinator;
    }

    public void setCoordinator(Coordinator coordinator) {
        this.coordinator = coordinator;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<Student> getStudentsList() {
        return students;
    }

    public void addStudentToLesson(Student student) {
        students.add(student);
    }

    public void removeStudentFromLesson(Student student) {
        int index = students.indexOf(student);
        if (index != -1) {
            students.remove(index);
        }
    }


}
