package com.demo;

import java.util.UUID;

public class Tutor extends Human {
    private UUID moneyAccount;
    private double salary;
    private Coordinator coordinator;

    public Tutor(String firstName, String lastName, String subject, double salary) {
        super(firstName, lastName, subject);
        this.moneyAccount = UUID.randomUUID();
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Coordinator getCoordinator() {
        return coordinator;
    }

    public void setCoordinator(Coordinator coordinator) {
        this.coordinator = coordinator;
    }

    public UUID getMoneyAccount() {
        return moneyAccount;
    }

    public void getInfo() {
        System.out.println("Salary: " + this.salary);
    }

}

