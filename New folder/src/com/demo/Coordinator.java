package com.demo;

public class Coordinator extends Human {
    public Coordinator(String firstName, String lastName, String subject) {
        super(firstName, lastName, subject);
    }

    public void getInfo() {
        System.out.println("Hello I have no info, I�m a coordinator.");
    }
}
