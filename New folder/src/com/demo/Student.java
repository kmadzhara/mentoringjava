package com.demo;

import java.util.UUID;

public class Student extends Human {
    private UUID moneyAccount;
    private double price;
    private String level;
    private Coordinator coordinator;

    public Student(String firstName, String lastName, String subject, double price) {
        super(firstName, lastName, subject);
        this.moneyAccount = UUID.randomUUID();
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Coordinator getCoordinator() {
        return coordinator;
    }

    public void setCoordinator(Coordinator coordinator) {
        this.coordinator = coordinator;
    }

    public UUID getMoneyAccount() {
        return moneyAccount;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public void getInfo() {
        System.out.println("Level: " + this.level);
    }
}
