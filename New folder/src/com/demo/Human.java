package com.demo;

public class Human {
    private String firstName;
    private String lastName;
    private String subject;
    
	public Human(String firstName, String lastName, String subject) {
	    this.firstName = firstName;
	    this.lastName = lastName;
	    this.subject = subject;
	    }

	public String getFirstName() {
	        return firstName;
	    }

	public String getLastName() {
	        return lastName;
	    }

	public String getSubject() {
	        return subject;
	    }

	public void setSubject(String subject) {
	        this.subject = subject;
	    }

	public abstract void getInfo();
	}


